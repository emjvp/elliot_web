<div class="inicio padding-content">
    <h1 class="titulo">
        <?php echo $this->intrproyecto->contenidos_titulo?>
    </h1>
    <h3 class="subtitulo">
        <?php echo $this->intrproyecto->contenidos_subtitulo?>
    </h3>
    <div class="introduccion">
        <?php echo $this->intrproyecto->contenidos_introduccion?>
    </div>
    <div class="descripcion">
        <?php echo $this->intrproyecto->contenidos_descripcion?>
    </div>
</div>
